require('dotenv-extended').load();

var restify = require('restify'),
    builder = require('botbuilder'),
    prompts = require('./messages');

var server = restify.createServer();
server.listen(process.env.port || process.env.PORT, function () {
    console.log('%s listening to %s', server.name, server.url);
});

var chatConnector = new builder.ChatConnector({
    appId: process.env.MICROSOFT_APP_ID,
    appPassword: process.env.MICROSOFT_APP_PASSWORD
});

var chatBot = new builder.UniversalBot(chatConnector);
chatBot.set('storage', new builder.MemoryBotStorage());

server.post('/api/messages', chatConnector.listen());

var wolfram = require('wolfram').createClient(process.env.WOLFRAM_API_KEY)

chatBot.dialog('/', function (session) {
    wolfram.query(session.message.text, function (err, result) {
        if (err || typeof result[1] === 'undefined')
            session.send(prompts.wolfram.failed);
        else if (result[1]['subpods'][0]['value'].length > 0)
            session.send(result[1]['subpods'][0]['value']);
        else if (result[1]['subpods'][0]['image'].length > 0)
            session.send(result[1]['subpods'][0]['image']);
        else
            session.send(prompts.wolfram.failed);
    });
});
